# cjson.build

cmake build cJSON build for MSVC or GNU C 

## Download

	git clone --recursive https://gitee.com/l0km/cjson.build.git

## Build Environment

cmake 

Visual Studio 2015 OR MinGW for windows

gcc for linux

## Visual Studio 2015

	msvc_build.bat

## GCC/MinGW

	gnu_build.sh