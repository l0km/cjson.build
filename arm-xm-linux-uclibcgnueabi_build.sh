#!/bin/bash
# arm-xm-linux-ulibcgnueabi(雄迈650) cJSON 交叉编译脚本
# Optional Command Arguments:
#    ONLYBUILD   不执行 cmake 创建工程只编译版本
#	 BUILD_UTILS build cjson utils
#	 BUILD_TEST  build test
# Optional Environment Variables: 
#    PREFIX 安装路径
#    PROJECT_FOLDER cmake 生成的工程文件(Makefile)文件夹
#    BUILD_TYPE 编译版本类型(DEBUG|RELEASE),默认 RELEASE

sh_folder=$(cd "$(dirname $0)"; pwd -P)

TOOLCHAIN_FILE=$sh_folder/cmake/arm-xm-linux-uclibcgnueabi.toolchain.cmake \
MACHINE=arm-xm-linux-uclibcgnueabi \
./gnu_build.sh $*
