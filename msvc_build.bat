:: 基于 Windows Visual Studio 2015 的 cJSON 编译脚本
:: 可选参数:
::   /S              - 编译单元测试
::   /U              - 编译CJSON_UTILS

@ECHO OFF
SETLOCAL
ECHO make paho mqtt VS2015 project
IF NOT DEFINED VS140COMNTOOLS (
	ECHO vs2015 NOT FOUND.
	EXIT /B -1
)
ECHO vs2015 found.
WHERE cmake
IF ERRORLEVEL 1 (
	ECHO cmake NOT FOUND.
	EXIT /B -1
)
ECHO cmake found
SET sh_folder=%~dp0
:: 删除最后的 '\'
SET sh_folder=%sh_folder:~0,-1%

SET BUILD_TEST=FALSE
SET BUILD_UTILS=FALSE
:: parse command arguments
:loop
IF x%1 == x GOTO :pare_end
IF /I "%1" == "/S"      SET BUILD_TEST=TRUE
IF /I "%1" == "/U"      SET BUILD_UTILS=TRUE

SHIFT
GOTO :loop
:pare_end

SET src_folder=cJSON
SET release_prefix=cJSON

SET cmake_prefix_path=

SET project_folder=%sh_folder%\build\%release_prefix%.vs2015
IF "%OUTDIR%x" == "x" SET OUTDIR=%sh_folder%\release\%release_prefix%_windows_vc_x86-64

ECHO project_folder=%project_folder%
ECHO OUTDIR=%OUTDIR%

:: 生成项目工程文件
IF EXIST %project_folder% RMDIR %project_folder% /s/q 
MKDIR %project_folder% 
PUSHD %project_folder% || EXIT /B

IF NOT DEFINED VisualStudioVersion (
	ECHO make MSVC environment ...
	CALL "%VS140COMNTOOLS%..\..\vc/vcvarsall" x86_amd64
)

ECHO creating x86_64 Project for Visual Studio 2015 ...
CMAKE %sh_folder%\%src_folder% -G "Visual Studio 14 2015 Win64" -DCMAKE_INSTALL_PREFIX=%OUTDIR% ^
    %cmake_prefix_path% ^
	-DBUILD_SHARED_AND_STATIC_LIBS=ON ^
	-DENABLE_CJSON_UTILS=%BUILD_UTILS% ^
    -DENABLE_CJSON_TEST=%BUILD_TEST% ^
    -DCMAKE_DEBUG_POSTFIX=_d ^
	-DENABLE_TARGET_EXPORT=ON  || EXIT /B

cmake --build . --config Release --target install -- /maxcpucount || EXIT /B
cmake --build . --config Debug   --target install -- /maxcpucount || EXIT /B
POPD
ENDLOCAL