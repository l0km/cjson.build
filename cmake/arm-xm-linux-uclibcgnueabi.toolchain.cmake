# This one is important
SET(CMAKE_SYSTEM_NAME Linux)
SET(CMAKE_SYSTEM_PROCESSOR arm)

set(_compiler_prefix /opt/xm_toolchain/arm-xm-linux/usr)
if(NOT EXISTS ${_compiler_prefix})
	if(NOT $ENV{CROSS_COMPILER_PREFIX} STREQUAL "")
	    set(_compiler_prefix $ENV{CROSS_COMPILER_PREFIX})
	elseif(CROSS_COMPILER_PREFIX)
	    set(_compiler_prefix ${CROSS_COMPILER_PREFIX})
	else()
		find_program(_gcc_path arm-xm-linux-uclibcgnueabi-gcc)
		if(NOT _gcc_path)
			message(FATAL_ERROR "NOT FOUND compiler arm-xm-linux-uclibcgnueabi-gcc in system path")
		endif()
		get_filename_component(_bin ${_gcc_path} DIRECTORY )
		get_filename_component(_compiler_prefix ${_bin} DIRECTORY )
	endif()	
endif()

# Specify the cross compiler
SET(CMAKE_C_COMPILER "${_compiler_prefix}/bin/arm-xm-linux-uclibcgnueabi-gcc")
SET(CMAKE_CXX_COMPILER "${_compiler_prefix}/bin/arm-xm-linux-uclibcgnueabi-g++")

# Where is the target environment
SET(CMAKE_FIND_ROOT_PATH "${_compiler_prefix}/arm-xm-linux-uclibcgnueabi/sysroot")

# Search for programs in the build host directories
SET(CMAKE_FIND_ROOT_PATH_MODE_PROGRAM NEVER)

# For libraries and headers in the target directories
SET(CMAKE_FIND_ROOT_PATH_MODE_LIBRARY BOTH)
SET(CMAKE_FIND_ROOT_PATH_MODE_INCLUDE BOTH)

unset(_compiler_prefix)
