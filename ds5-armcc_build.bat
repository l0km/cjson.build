:: 基于 armcc 的 cJSON 编译脚本
:: 可选参数:
::   /S              - 编译单元测试
::   /U              - 编译CJSON_UTILS

@ECHO OFF
SETLOCAL
ECHO make cjson armcc project
WHERE armcc
if errorlevel 1 (
	ECHO armcc NOT FOUND.
	exit /B -1
)
ECHO armcc found
WHERE cmake
IF ERRORLEVEL 1 (
	ECHO cmake NOT FOUND.
	EXIT /B -1
)
ECHO cmake found
SET sh_folder=%~dp0
:: 删除最后的 '\'
SET sh_folder=%sh_folder:~0,-1%

SET BUILD_TEST=FALSE
SET BUILD_UTILS=FALSE
:: parse command arguments
:loop
IF x%1 == x GOTO :pare_end
IF /I "%1" == "/S"      SET BUILD_TEST=TRUE
IF /I "%1" == "/U"      SET BUILD_UTILS=TRUE

SHIFT
GOTO :loop
:pare_end

:: 定义编译的版本类型(DEBUG|RELEASE)
SET build_type=RELEASE

SET src_folder=cJSON
SET release_prefix=cJSON

SET cmake_prefix_path=

SET project_folder=%sh_folder%\build\%release_prefix%.armcc
IF "%OUTDIR%x" == "x" SET OUTDIR=%sh_folder%\release\%release_prefix%_arm-linux-armcc

ECHO project_folder=%project_folder%
ECHO OUTDIR=%OUTDIR%

:: 生成项目工程文件
IF EXIST %project_folder% RMDIR %project_folder% /s/q 
MKDIR %project_folder% 
PUSHD %project_folder% || EXIT /B

ECHO creating x86_64 Project for armcc ...
SET CC=armcc
CMAKE %sh_folder%\%src_folder% -G "Eclipse CDT4 - Unix Makefiles" -DCMAKE_INSTALL_PREFIX=%OUTDIR% ^
    %cmake_prefix_path% ^
  -DCMAKE_TOOLCHAIN_FILE=%sh_folder%\cmake\ds5-arm-linux-armcc.toolchain.cmake ^
	-DBUILD_SHARED_AND_STATIC_LIBS=ON ^
	-DENABLE_CJSON_UTILS=%BUILD_UTILS% ^
  -DENABLE_CJSON_TEST=%BUILD_TEST% ^
  -DCMAKE_DEBUG_POSTFIX=_d ^
  -DCMAKE_BUILD_TYPE=%build_type% ^
	-DENABLE_TARGET_EXPORT=ON  || EXIT /B

cmake --build .  --target install || EXIT /B
POPD
ENDLOCAL